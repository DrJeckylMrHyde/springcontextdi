package pl.javastart.beans.app;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.javastart.beans.MessagePrinter;

public class SpringApplicationDI {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        MessagePrinter printer = context.getBean(MessagePrinter.class);
        printer.printMessage();
        context.close();
    }
}
